package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.api.mvc.Handler;
import play.libs.F;
import play.libs.Json;
import play.libs.ws.WS;
import play.libs.ws.WSRequestHolder;
import play.libs.ws.WSResponse;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import scala.concurrent.Future;
import services.HotelSearchMock;
import services.WSParameters;
import services.WSRequestFactory;
import akka.dispatch.Futures;

/**
 * Created by matei_000 on 5/31/2015.
 */
public class HotelController extends Controller {

    final static  String wsBaseURL = "https://dev.api.ean.com";
    final  static String wsHotelsListPath = "/ean-services/rs/hotel/v3/list";
    final  static String wsCommonRequest = "cid=55505&apiKey=jv9v72vx8v33a9tsg6tt5vqt&locale=en_US&curencyCode=USD&minorRev=99&apiExperience=PARTNER_WEBSITE";
    final static String wsHotelInfo = "/ean-services/rs/hotel/v3/info";
    final static String wsRoomAvailability = "/ean-services/rs/hotel/v3/avail";
    final static String wsBookHotel = "https://book.api.ean.com/ean-services/rs/hotel/v3/res";

    @BodyParser.Of(BodyParser.Json.class)
    public static F.Promise<Result> getHotels(){

        String url = wsBaseURL + wsHotelsListPath;

        String destination = request().getQueryString("address");
        String arrivalDate = request().getQueryString("checkin");
        String departureDate = request().getQueryString("checkout");
        String ipAddress = request().remoteAddress();
        String customerSessionId = request().getQueryString("customerSessionId");

        String queryString = "&destinationString="+destination + "&arrivalDate=" + arrivalDate + "&departureDate=" + departureDate; //+"&customerIpAddress=" + ipAddress;
        Logger.info(queryString);

        //

        if(StringUtils.isEmpty(destination) || StringUtils.isEmpty(arrivalDate) || StringUtils.isEmpty(departureDate) ){
             throw new RuntimeException("Destination arrival date or departure date are empty");

        }

        session("ArrivalDate", arrivalDate);
        session("DepartureDate", departureDate);


        F.Promise<WSResponse> responsePromise = WS.url(url).setQueryString(wsCommonRequest+queryString).setContentType("application/json").get();

        responsePromise.onFailure(throwable -> {
            Logger.error("error", throwable);
            throw new RuntimeException(throwable.toString());
        });

        return responsePromise.map(response ->{

                    String retVal = response.asJson().toString();
                    Logger.info("Value from EAN " +retVal);
                    Logger.info("Body " + response.getBody());
                   // Logger.info(Json.parse(response.getBody()).toString());
                    String customerId = response.asJson().findValue("customerSessionId").textValue();

                    if(customerId != null){
                        session("customerSessionId", customerId );
                    } else{
                        Logger.error("Cannot find customer sessionID in the response");
                    }

                    return ok( retVal);
                }
        );

    }

    public static F.Promise<Result> makeBooking(){

        Logger.info("Make booking request " +request().body().asJson().asText());

        JsonNode req = request().body().asJson();

        String hotelId = req.get("hotelId").asText();
        String arrivalDate = req.get("arrivalDate").asText();
        String departureDate = req.get("departureDate").asText();
        String supplierType = req.get("supplierType").asText();
        String rateKey = req.get("rateKey").asText();
        String roomTypeCode = req.get("roomTypeCode").asText();
        String rateCode = req.get("rateCode").asText();
        String chargeableRate = req.get("chargeableRate").asText();
        String email = req.get("email").asText();
        String firstName = req.get("firstName").asText();
        String lastName = req.get("lastName").asText();
        String homePhone = "012345678";//req.get("homePhone").asText();
        String creditCardType = "CA";//req.get("creditCardType").asText();
        String creditCardNumber = "5401999999999999";//req.get("creditCardNumber").asText();
        String creditCardIdentifier = "123";//req.get("creditCardIdentifier").asText();
        String creditCardExpirationMonth  = "08";//req.get("creditCardExpirationMonth ").asText();
        String creditCardExpirationYear = "2019"; //req.get("creditCardExpirationYear").asText();
        String address1 = "travelnow";
        String bedTypeId = req.get("bedTypeId").asText();

        String ipAddress = request().remoteAddress();
        String customerSessionId = session("customerSessionId");
        String userAgent = request().headers().get("User-Agent").toString();


        String query =  "&hotelId=" + hotelId+"&customerSessionId=" +customerSessionId + "&customerIpAddress=" + ipAddress; //+ "&customerUserAgent=" + userAgent;
        query += "&arrivalDate=" + arrivalDate + "&departureDate=" + departureDate + "&room1=2&room1FirstName=TestBooking" +
                "&room1LastName=TestBooking" +
                "&room1BedTypeId=" + bedTypeId +
                "&room1SmokingPreference=NS&supplierType=" + supplierType +
                "&rateKey=" +rateKey +
                "&roomTypeCode=" + roomTypeCode +
                "&rateCode=" + rateCode +
                "&chargeableRate=" + chargeableRate +
                "&email=" + email +
                "&firstName=" + firstName +
                "&lastName=" + lastName +
                "&homePhone=" + homePhone +
                "&workPhone=" + homePhone +
                "&creditCardType=" + creditCardType +
                "&creditCardNumber=" + creditCardNumber +
                "&creditCardIdentifier=" + creditCardIdentifier+
                "&creditCardExpirationMonth=" + creditCardExpirationMonth +
                "&creditCardExpirationYear=" + creditCardExpirationYear +
                "&address1=travelnow" +
                "&city=Bellevue" +
                "&stateProvinceCode=WA" +
                "&countryCode=US" +
                "&postalCode=98004";

        //String usl = wsBookHotel + "?" + wsCommonRequest + query;
        String p = wsCommonRequest + query;
        Logger.info(p);

        F.Promise<WSResponse> responsePromise = WS.url(wsBookHotel).setQueryString(p).setContentType("application/json").setFollowRedirects(true).setMethod("POST").post(p);

        responsePromise.onFailure(throwable -> {
            Logger.error("error", throwable);
            throw new RuntimeException(throwable.toString());
        });

        return responsePromise.map(response ->{

                    String retVal = response.asJson().toString();
                    Logger.info("Booking" +retVal);
                    Logger.info("Body Booking " + response.getBody());


                    return ok( retVal);
                }
        );

    }

    public static F.Promise<Result> getRoomAvailability(){

        String url = wsBaseURL + wsRoomAvailability;

        String hotelId = request().getQueryString("hotelId");
        String ipAddress = request().remoteAddress();
        String customerSessionId = session("customerSessionId");
       String userAgent = request().headers().get("User-Agent").toString();

        String arrivalDate = session("ArrivalDate");
        String departureDate = session("DepartureDate");

        String query =  "&hotelId=" + hotelId+"&customerSessionId=" +customerSessionId + "&customerIpAddress=" + ipAddress; //+ "&customerUserAgent=" + userAgent;
        query += "&arrivalDate=" + arrivalDate + "&departureDate=" + departureDate + "&room1=2&includeImages=true";

        Logger.info(query);

        WSRequestHolder wsRequest = WSRequestFactory.roomAvailabilityRequest(url, hotelId, customerSessionId, ipAddress, userAgent, arrivalDate, departureDate).setContentType("application/json");

        //Logger.info(wsRequest.getQueryParameters().toString());
        F.Promise<WSResponse> responsePromise = wsRequest.get();
      //  F.Promise<WSResponse> responsePromise = WS.url(url).setQueryString(wsCommonRequest + query).setContentType("application/json").get();
        responsePromise.onFailure(throwable -> {
            Logger.error("error", throwable);

            throw new RuntimeException(throwable.toString());
        });

        return responsePromise.map(response ->{
                    String retVal = response.asJson().toString();
                    Logger.info("RoomAvail " +retVal);
                    Logger.info("Body RoomAvail" + response.getBody());
                    // Logger.info(Json.parse(response.getBody()).toString());
                    return ok( response.asJson());
                }
        );

    }

    public static F.Promise<Result> getHotelInfo(){

        String url = wsBaseURL + wsHotelInfo;

        String hotelId = request().getQueryString("hotelId");
        String ipAddress = request().remoteAddress();
        String customerSessionId = session("customerSessionId");
        String userAgent = request().headers().get("User-Agent").toString();

        String query =  "&hotelId=" + hotelId+"&customerSessionId=" +customerSessionId + "&customerIpAddress=" + ipAddress + "&customerUserAgent=" + userAgent;

        Logger.info(query);
        F.Promise<WSResponse> responsePromise = WS.url(url).setQueryString(wsCommonRequest +query).setContentType("application/json").get();

        responsePromise.onFailure(throwable -> {
            Logger.error("error", throwable);
            throw new RuntimeException(throwable.toString());
        });

        return responsePromise.map(response ->{
                    String retVal = response.asJson().toString();
                    Logger.info("Value from EAN " +retVal);
                    Logger.info("Body " + response.getBody());
                   // Logger.info(Json.parse(response.getBody()).toString());
                    return ok( response.asJson());
                }
        );

    }
}
