package controllers;

import play.Logger;
import play.api.Play;
import play.libs.F;
import play.libs.F.Function;
import play.libs.Json;
import play.libs.ws.*;
import play.mvc.*;
import views.html.index;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Application extends Controller {

    public static Result index(String any) {
        return ok(index.render());
    }


    /** resolve "any" into the corresponding HTML page URI */
    private static String getURI(String any) {
        switch (any) {
            case  "HotelSearch" : return "/public/angular/views/template/" + any.toLowerCase() + ".html";


            default: return "/public/angular/views/" + any.toLowerCase() + ".html";
        }
    }

    /** load an HTML page from public/html */
    public static Result loadPublicHTML(String any) {

        Logger.info("Request to server view " + any);
        File projectRoot = Play.current().path();
        File file = new File(projectRoot + getURI(any));
        if (file.exists()) {
            try {
                Logger.info("Serving file " + file.getCanonicalPath());
                return ok(file).as("text/html");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return notFound();
    }

}
