package services;

import com.fasterxml.jackson.databind.JsonNode;
import play.api.libs.json.Json;
import play.libs.F;
import services.Interfaces.IHotelsListService;
import play.Logger;

/**
 * Created by matei_000 on 3/24/2015.
 */
public class HotelListService extends ServiceBase implements IHotelsListService {


    @Override
    public F.Promise<JsonNode> getHotels() {
        Logger.info("getHotels");
        F.Promise<JsonNode> val = makeRequest(0);

        Logger.info("Returned " + val);
        return val;
    }


}
