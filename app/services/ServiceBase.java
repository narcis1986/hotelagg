package services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import play.Logger;
import play.libs.F;
import play.libs.Json;
import play.libs.ws.*;
import play.libs.F.Promise;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by matei_000 on 3/24/2015.
 */
public class ServiceBase {
//    final  String wsBaseURL = Play.current().configuration().getString("ean.webservice",null).get();
//    final  String wsHotelsListPath = Play.current().configuration().getString("ean.hotelsPath",null).get();
//    final  String wsCommonRequest = Play.current().configuration().getString("ean.commonRequest",null).get();
    final static  String wsBaseURL = "https://dev.api.ean.com";
    final  static String wsHotelsListPath = "/ean-services/rs/hotel/v3/list";
    final  static String wsCommonRequest = "cid=55505&apiKey=jv9v72vx8v33a9tsg6tt5vqt&locale=en_US&curencyCode=EUR&minorRev=99&city=Craiova";

    protected Promise<JsonNode> makeRequest(int requestType){

        if(requestType == 0){
            String url = wsBaseURL + wsHotelsListPath;

            Logger.info(url);

            Promise<WSResponse> responsePromise = WS.url(url).setQueryString(wsCommonRequest).setContentType("application/json").get();

            responsePromise.onFailure(throwable -> {
                Logger.error("error", throwable);
                throw new RuntimeException(throwable.toString());
            });

            responsePromise.map(response -> {
                String retVal = response.asJson().toString();
                Logger.info("Value from EAN " +retVal);
                Logger.info("Body " + response.getBody());
                Logger.info(Json.parse(response.getBody()).toString());
                return response.asJson();
            });

        }
        return null;
    }
}
