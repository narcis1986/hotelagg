package services;

import play.libs.ws.WS;
import play.libs.ws.WSRequestHolder;

/**
 * Created by matei_000 on 6/8/2015.
 */
public class WSRequestFactory {

   // private String url;

   public static WSRequestHolder roomAvailabilityRequest (String url, String hotelId, String customerSessionId, String ipAddress,
                                                          String userAgent, String arrivalDate, String departureDate){

       WSRequestHolder wsRequest = WS.url(url);

       createCommonRequest(wsRequest);

       wsRequest.setQueryParameter(WSParameters.HotelId, hotelId);
       wsRequest.setQueryParameter(WSParameters.CustomerSessionId, customerSessionId);
       wsRequest.setQueryParameter(WSParameters.CustomerIpAddress, ipAddress);
       wsRequest.setQueryParameter(WSParameters.CustomerUserAgent, userAgent);
       wsRequest.setQueryParameter(WSParameters.ArrivalDate, arrivalDate);
       wsRequest.setQueryParameter(WSParameters.DepartureDate, departureDate);
       wsRequest.setQueryParameter("room1", "2");
       wsRequest.setQueryParameter("includeRoomImages", "true");
       wsRequest.setQueryParameter("options", "HOTEL_DETAILS,ROOM_TYPES,HOTEL_IMAGES");

       return wsRequest;

    }

    private static void createCommonRequest(WSRequestHolder req){
        req.setQueryParameter(WSParameters.CID, "55505");
        req.setQueryParameter(WSParameters.ApiKey, "jv9v72vx8v33a9tsg6tt5vqt");
        req.setQueryParameter(WSParameters.Locale, "en_US");
        req.setQueryParameter(WSParameters.CurrencyCode, "EUR");
        req.setQueryParameter(WSParameters.ApiExperience, "PARTNER_WEBSITE");

    }
}
