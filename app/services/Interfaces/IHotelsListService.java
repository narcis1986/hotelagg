package services.Interfaces;

import com.fasterxml.jackson.databind.JsonNode;
import play.api.libs.json.Json;
import play.libs.F;

/**
 * Created by matei_000 on 3/24/2015.
 */
public interface IHotelsListService {
    public F.Promise<JsonNode> getHotels();
}
