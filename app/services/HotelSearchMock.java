package services;

import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Json;

/**
 * Created by matei_000 on 6/14/2015.
 */
public class HotelSearchMock {

    public JsonNode getHotelsList(){
        JsonNode node = Json.parse("{\n" +
                "\"HotelListResponse\": {\n" +
                "    \"customerSessionId\": \"0AB29030-103C-A914-D0B2-99F74F907B31\",\n" +
                "    \"numberOfRoomsRequested\": 1,\n" +
                "    \"moreResultsAvailable\": true,\n" +
                "    \"cacheKey\": \"-517103ca:14d0b99f74f:-7b27\",\n" +
                "    \"cacheLocation\": \"10.178.144.48:7300\",\n" +
                "    \"cachedSupplierResponse\": {\n" +
                "        \"@supplierCacheTolerance\": \"NOT_SUPPORTED\",\n" +
                "        \"@cachedTime\": \"0\",\n" +
                "        \"@supplierRequestNum\": \"231\",\n" +
                "        \"@supplierResponseNum\": \"25\",\n" +
                "        \"@supplierResponseTime\": \"576\",\n" +
                "        \"@candidatePreptime\": \"27\",\n" +
                "        \"@otherOverheadTime\": \"5\",\n" +
                "        \"@tpidUsed\": \"5001\",\n" +
                "        \"@matchedCurrency\": \"true\",\n" +
                "        \"@matchedLocale\": \"true\"\n" +
                "    },\n" +
                "    \"HotelList\": {\n" +
                "        \"@size\": \"25\",\n" +
                "        \"@activePropertyCount\": \"231\",\n" +
                "        \"HotelSummary\": [\n" +
                "            {\n" +
                "                \"@order\": \"0\",\n" +
                "                \"@ubsScore\": \"371858453\",\n" +
                "                \"hotelId\": 119562,\n" +
                "                \"name\": \"Hotel Andra\",\n" +
                "                \"address1\": \"2000 4th Ave\",\n" +
                "                \"city\": \"Seattle\",\n" +
                "                \"stateProvinceCode\": \"WA\",\n" +
                "                \"postalCode\": 98121,\n" +
                "                \"countryCode\": \"US\",\n" +
                "                \"airportCode\": \"SEA\",\n" +
                "                \"supplierType\": \"E\",\n" +
                "                \"propertyCategory\": 1,\n" +
                "                \"hotelRating\": 4,\n" +
                "                \"confidenceRating\": 45,\n" +
                "                \"amenityMask\": 1446211,\n" +
                "                \"locationDescription\": \"Near Pike Place Market\",\n" +
                "                \"shortDescription\": \"&lt;p&gt;&lt;b&gt;Property Location&lt;/b&gt; &lt;br /&gt;With a stay at Hotel Andra, you&apos;ll be centrally located in Seattle, minutes from Westlake Center and Pike Place Market. This 4-star hotel is within close proximity\",\n" +
                "                \"highRate\": 509,\n" +
                "                \"lowRate\": 509,\n" +
                "                \"rateCurrencyCode\": \"USD\",\n" +
                "                \"latitude\": 47.61318,\n" +
                "                \"longitude\": -122.34009,\n" +
                "                \"proximityDistance\": 11.438348,\n" +
                "                \"proximityUnit\": \"MI\",\n" +
                "                \"hotelInDestination\": true,\n" +
                "                \"thumbNailUrl\": \"/hotels/1000000/20000/17000/16950/16950_185_t.jpg\",\n" +
                "                \"deepLink\": \"http://www.travelnow.com/templates/369532/hotels/119562/overview?lang=en&amp;currency=USD&amp;standardCheckin=9/4/2015&amp;standardCheckout=9/5/2015&amp;roomsCount=1&amp;rooms[0].adultsCount=2\",\n" +
                "                \"RoomRateDetailsList\": {\n" +
                "                    \"RoomRateDetails\": {\n" +
                "                        \"roomTypeCode\": 254862,\n" +
                "                        \"rateCode\": 680894,\n" +
                "                        \"maxRoomOccupancy\": 2,\n" +
                "                        \"quotedRoomOccupancy\": 2,\n" +
                "                        \"minGuestAge\": 0,\n" +
                "                        \"roomDescription\": \"Andra Room, 1 Queen Bed\",\n" +
                "                        \"propertyAvailable\": true,\n" +
                "                        \"propertyRestricted\": false,\n" +
                "                        \"expediaPropertyId\": 16950,\n" +
                "                        \"RateInfos\": {\n" +
                "                            \"@size\": \"1\",\n" +
                "                            \"RateInfo\": {\n" +
                "                                \"@priceBreakdown\": \"true\",\n" +
                "                                \"@promo\": \"false\",\n" +
                "                                \"@rateChange\": \"false\",\n" +
                "                                \"RoomGroup\": {\n" +
                "                                    \"Room\": {\n" +
                "                                        \"numberOfAdults\": 2,\n" +
                "                                        \"numberOfChildren\": 0,\n" +
                "                                        \"rateKey\": \"ae6fc691-e5d9-4579-9b51-cc6c77e1e940\",\n" +
                "                                        \"ChargeableNightlyRates\": {\n" +
                "                                            \"@baseRate\": \"509.00\",\n" +
                "                                            \"@rate\": \"509.00\",\n" +
                "                                            \"@promo\": \"false\"\n" +
                "                                        }\n" +
                "                                    }\n" +
                "                                },\n" +
                "                                \"ChargeableRateInfo\": {\n" +
                "                                    \"@averageBaseRate\": \"509.00\",\n" +
                "                                    \"@averageRate\": \"509.00\",\n" +
                "                                    \"@commissionableUsdTotal\": \"509.00\",\n" +
                "                                    \"@currencyCode\": \"USD\",\n" +
                "                                    \"@maxNightlyRate\": \"509.00\",\n" +
                "                                    \"@nightlyRateTotal\": \"509.00\",\n" +
                "                                    \"@surchargeTotal\": \"93.52\",\n" +
                "                                    \"@total\": \"602.52\",\n" +
                "                                    \"NightlyRatesPerRoom\": {\n" +
                "                                        \"@size\": \"1\",\n" +
                "                                        \"NightlyRate\": {\n" +
                "                                            \"@baseRate\": \"509.00\",\n" +
                "                                            \"@rate\": \"509.00\",\n" +
                "                                            \"@promo\": \"false\"\n" +
                "                                        }\n" +
                "                                    },\n" +
                "                                    \"Surcharges\": {\n" +
                "                                        \"@size\": \"1\",\n" +
                "                                        \"Surcharge\": {\n" +
                "                                            \"@type\": \"TaxAndServiceFee\",\n" +
                "                                            \"@amount\": \"93.52\"\n" +
                "                                        }\n" +
                "                                    }\n" +
                "                                },\n" +
                "                                \"nonRefundable\": false,\n" +
                "                                \"rateType\": \"MerchantStandard\",\n" +
                "                                \"currentAllotment\": 10\n" +
                "                            }\n" +
                "                        }\n" +
                "                    }\n" +
                "                }\n" +
                "            }\n");

            return node;
    }

}
