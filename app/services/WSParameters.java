package services;

/**
 * Created by matei_000 on 6/8/2015.
 */
public class WSParameters {
    public static final String HotelId = "hotelId";
    public static final String CustomerSessionId = "customerSessionId";
    public static final String ArrivalDate= "arrivalDate";
    public static final String DepartureDate= "departureDate";
    public static final String CustomerIpAddress= "customerIpAddress";
    public static final String CustomerUserAgent= "customerUserAgent";

    //common request query parameters
    public static final String CID = "CID";
    public static final String ApiKey= "apiKey";
    public static final String Locale= "locale";
    public static final String CurrencyCode= "curencyCode";
    public static final String MinorRev= "minorRev";
    public static final String ApiExperience= "apiExperience";



}
