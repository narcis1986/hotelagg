/**
 * Created by matei_000 on 6/1/2015.
 */

angular.module("hotelAgg").controller('BookHotelController', function($scope, $location, $http, $sce, HotelsList, Lightbox){

    $scope.HotelInfo = HotelsList.HotelInfo;
    $scope.RoomsList = HotelsList.HotelInfo.HotelRoomAvailabilityResponse.HotelRoomResponse;
    $scope.Room = HotelsList.Room;

    function parseDate(str) {
        var mdy = str.split('/')
        return new Date(mdy[2], mdy[0]-1, mdy[1]);
    }

    function daydiff(first, second) {
        return (second-first)/(1000*60*60*24);
    }


    $scope.formatedArrivalDate = function(){
        return parseDate($scope.Room.arrivalDate).toDateString();
    }


    $scope.formatedDepartuDate = function(){
        return parseDate($scope.Room.departureDate).toDateString();
    }

    $scope.getNrOfNights = function(){
        return daydiff(parseDate($scope.Room.arrivalDate), parseDate($scope.Room.departureDate));
    }

    $scope.makeBooking = function(){

        console.log($scope.booking);

        var book = $scope.booking;
        book.hotelId = $scope.Room.hotelId;
        book.arrivalDate = $scope.Room.arrivalDate;
        book.departureDate = $scope.Room.departureDate;
        book.supplierType = $scope.Room.supplierType;
        book.rateKey = $scope.Room.rateKey;
        book.roomTypeCode = $scope.Room.roomTypeCode;
        book.rateCode = $scope.Room.rateCode;
        if($scope.Room.BedTypes.BedType["@id"] != null )
            book.bedTypeId = $scope.Room.BedTypes.BedType["@id"];
        else
            book.bedTypeId = $scope.Room.BedTypes.BedType[0]["@id"];
        book.chargeableRate = $scope.Room.RateInfo.ChargeableRateInfo["@total"];
        var jon = JSON.stringify($scope.booking);
        console.log(jon);
        var params;
        return $http.post('/api/book', jon).success(function(data, status){
            //var hotelsSummary = data.HotelListResponse.HotelList.HotelSummary;
            HotelsList.SuccessPayment = data;
            if(data.HotelRoomReservationResponse.confirmationNumbers != null){
                HotelsList.Booking = book;
                $location.path("/success_payment");
            }

            else if(data.HotelRoomReservationResponse.EanWsError != null)
                alert(data.HotelRoomReservationResponse.EanWsError.presentationMessage)
            else
                alert("There was an error!")
        } )


    }


})
