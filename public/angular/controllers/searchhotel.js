/**
 * Created by matei_000 on 6/1/2015.
 */

angular.module("hotelAgg").controller('SearchHotelController', function($scope, $location, $templateCache, $http, HotelsList) {

    $scope.date = {startDate: null, endDate: null};
    $scope.getAddress = function(viewValue) {

        var params = {address: viewValue, sensor: false};
        return $http.get('http://maps.googleapis.com/maps/api/geocode/json', {params: params})
            .then(function(res) {
                return res.data.results;
            });
    };

    $scope.getHotels= function(){
      //  this.selectedAddress = "London, UK";
      //  this.date.startDate = "06/17/2015";
      //  this.date.endDate = "06/20/2015";
        var params = {address: this.selectedAddress, checkin: this.date.startDate.format("MM/DD/YYYY"), checkout: this.date.endDate.format("MM/DD/YYYY")  }
      //  var params = {address: "London, UK", checkin: "09/20/2015", checkout: "09/24/2015"  }
        return $http.get('/api/getHotels', {params:params}).success(function(data, status){
            var hotelsSummary = data.HotelListResponse.HotelList.HotelSummary;
            HotelsList.data = hotelsSummary;
             if(data.HotelListResponse.EanWsError != null)
                alert(data.HotelListResponse.EanWsError.presentationMessage)
            else
                 $location.path("/getHotels");

        } )
    };

});
