/**
 * Created by matei_000 on 6/1/2015.
 */

angular.module("hotelAgg").controller('HotelInfoController', function($scope, $location, $http, $sce, HotelsList, Lightbox){

    $scope.HotelInfo = HotelsList.HotelInfo;

    console.log($scope.HotelInfo);

    // That's how the html is converted back
    var fixedData = angular.element('<textarea />').html(HotelsList.HotelInfo.HotelInformationResponse.HotelDetails.propertyDescription).text();

    $scope.HotelInfo.propertyDescription = $sce.trustAsHtml(fixedData);

    $scope.Images = HotelsList.HotelInfo.HotelInformationResponse.HotelImages.HotelImage;
    $scope.map = { center: { latitude: HotelsList.HotelInfo.HotelInformationResponse.HotelSummary.latitude, longitude: HotelsList.HotelInfo.HotelInformationResponse.HotelSummary.longitude }, zoom: 8 };

    $scope.openLightboxModal = function (index) {
        Lightbox.openModal($scope.Images, index);
    };

})
