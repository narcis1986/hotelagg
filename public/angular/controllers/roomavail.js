/**
 * Created by matei_000 on 6/1/2015.
 */

angular.module("hotelAgg").controller('RoomAvailController', function($scope, $location, $http, $sce, HotelsList, Lightbox){

    $scope.HotelInfo = HotelsList.HotelInfo;
    $scope.RoomsList = HotelsList.HotelInfo.HotelRoomAvailabilityResponse.HotelRoomResponse

    //for(var i = 0; i < $scope.RoomsList.length; i++ ){
    //    var room = $scope.RoomsList[i];
    //    console.log(room.RoomImages.RoomImage.url)
    //    console.log(room.RateInfo.ChargeableRateInfo["@total"])
    //}

    $scope.bookHotel = function(room){
        HotelsList.Room = room;
        HotelsList.Room.hotelId = $scope.HotelInfo.HotelRoomAvailabilityResponse.hotelId;
        HotelsList.Room.hotelName = $scope.HotelInfo.HotelRoomAvailabilityResponse.hotelName;
        HotelsList.Room.arrivalDate = $scope.HotelInfo.HotelRoomAvailabilityResponse.arrivalDate;
        HotelsList.Room.departureDate = $scope.HotelInfo.HotelRoomAvailabilityResponse.departureDate;
        HotelsList.Room.rateKey = $scope.HotelInfo.HotelRoomAvailabilityResponse.rateKey;
        $location.path("/bookHotel");
    }

    //console.log($scope.HotelInfo);
    //
    //// That's how the html is converted back
    //var fixedData = angular.element('<textarea />').html(HotelsList.HotelInfo.HotelInformationResponse.HotelDetails.propertyDescription).text();
    //
    //$scope.HotelInfo.propertyDescription = $sce.trustAsHtml(fixedData);
    //
    //$scope.Images = HotelsList.HotelInfo.HotelInformationResponse.HotelImages.HotelImage;
    //$scope.map = { center: { latitude: HotelsList.HotelInfo.HotelInformationResponse.HotelSummary.latitude, longitude: HotelsList.HotelInfo.HotelInformationResponse.HotelSummary.longitude }, zoom: 8 };
    //
    //$scope.openLightboxModal = function (index) {
    //    Lightbox.openModal($scope.Images, index);
    //};

})
