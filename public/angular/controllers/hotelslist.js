/**
 * Created by matei_000 on 6/1/2015.
 */


angular.module("hotelAgg").controller('HotelListController', function($scope, $location, $http, HotelsList){
    $scope.HotelsList = HotelsList.data

    var currency_symbols = {
        'USD': '$', // US Dollar
        'EUR': '�', // Euro
        'CRC': '?', // Costa Rican Col�n
        'GBP': '�', // British Pound Sterling
        'ILS': '?', // Israeli New Sheqel
        'INR': '?', // Indian Rupee
        'JPY': '�', // Japanese Yen
        'KRW': '?', // South Korean Won
        'NGN': '?', // Nigerian Naira
        'PHP': '?', // Philippine Peso
        'PLN': 'z?', // Polish Zloty
        'PYG': '?', // Paraguayan Guarani
        'THB': '?', // Thai Baht
        'UAH': '?', // Ukrainian Hryvnia
        'VND': '?', // Vietnamese Dong
    };

    $scope.getStars = function(stars){
        return new Array(Math.ceil(stars))
    }

    $scope.currencySymbol = function(curencyName){
        if(currency_symbols[curencyName]!==undefined) {
            return currency_symbols[curencyName];
        }
    }

    $scope.getMinRate = function(hotel){
        return this.currencySymbol(hotel.rateCurrencyCode) + hotel.lowRate;
    }

    $scope.getPicture = function(url){
        return url.substring(0, url.length-5) + "b.jpg";
    }

    $scope.getHotelInfo = function(hotelId){
        var params = {hotelId : hotelId}
        return $http.get('/api/roomavail', {params:params}).success(function(data, status){
            // console.log(JSON.stringify(data));
            //var hotelsSummary = data.HotelListResponse.HotelList.HotelSummary;
            HotelsList.HotelInfo = data;
            $scope.HotelInfo = data;
            if(data.HotelRoomAvailabilityResponse.EanWsError != null)
                alert(data.HotelRoomAvailabilityResponse.EanWsError.presentationMessage)
            else
                $location.path("/roomavail");
        } )
    }
})
