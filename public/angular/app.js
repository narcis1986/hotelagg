/**
 * Created by matei_000 on 6/1/2015.
 */

var myApp = angular.module('hotelAgg', ['ngAnimate',
    'ngRoute',
    'ui.bootstrap',
    'angular-loading-bar',
    'bootstrapLightbox',
    'mgcrea.ngStrap',
    'ngSanitize',

    'daterangepicker',
    'uiGmapgoogle-maps'])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/HotelSearch',
                controller: 'SearchHotelController'
            })
            .when('/getHotels', {
                templateUrl: 'views/HotelsList',
                controller: 'HotelListController'
            })
            .when('/getHotelInfo', {
                templateUrl: 'views/hotelinfo',
                controller: 'HotelInfoController'
            })
            .when('/roomavail', {
                templateUrl: 'views/roomavail',
                controller: 'RoomAvailController'
            })
            .when('/bookHotel', {
                templateUrl: 'views/template/bookhotel',
                controller: 'BookHotelController'
            })
            .when('/success_payment', {
                templateUrl: 'views/success_payment',
                controller: 'SuccessPaymentController'
            })
            .otherwise({
                redirectTo: '/'
            });
    }   ).config(function($locationProvider) {
        return $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        }).hashPrefix("!"); // enable the new HTML5 routing and history API
    }).config(function(uiGmapGoogleMapApiProvider) {
        uiGmapGoogleMapApiProvider.configure({
            //    key: 'your api key',
            v: '3.17',
            libraries: 'weather,geometry,visualization'
        });
    });

myApp.service("HotelsList", function HotelsList(){
    var HotelsList = this
})